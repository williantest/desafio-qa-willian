require 'singleton'

class Talk	
	
	include Singleton	
    
	attr_accessor :message

	def message_select()
		@message = query("message",:text)
		touch("context index:'"+(rand() * @message.length).to_i+"'")#select message total and randomize
	end
end
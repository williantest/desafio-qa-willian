Given(/^I'm talking with "(.*?)" at chat$/) do |context|    
  touch(context)#select one friend
end

Given(/^I've message at chat$/) do     
  if query("chat").empty?
  	steps %{I write a "test message"}
  end
end

When(/^I write a "(.*?)"$/) do |context|
  query("input", setText:context)
end

When(/^I touch in "(.*?)"$/) do |context|
  touch(context)
end

When(/^I select "(.*?)"$/) do |context|
  Talk.instance.message_select()
end
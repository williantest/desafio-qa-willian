RULES={}
class CheckOut

  attr_accessor :rules,:a_item,:b_item,:c_item,:d_item,:a_count,:b_count,:c_count,:d_count,:subtotal,:total,:item_price

	def initialize(rules)
		@rules = rules
		@subtotal = 0
		@total = 0
		@item_price = 0
		@a_item = 0
		@b_item = 0
		@c_item = 0
		@d_item = 0
		@a_count=1
		@b_count=1
		@c_count=1
		@d_count=1    
	end

  def scan(item)
    case item
    when "A"
      if @a_count % 3 == 0
        @a_item=30
      else
        @a_item=50
      end
      @a_count +=1
      @item_price = @a_item
    when "B"
      if @b_count % 2 == 0
        @b_item=15
      else
        @b_item=30
      end
      @b_count +=1
      @item_price = @b_item
    when "C"
      @item_price = 20
    when "D"
      @item_price = 15
    else
      @item_price = 0
    end
      @subtotal += @item_price
  end

  def total()
    @total = @subtotal
  end 
 end
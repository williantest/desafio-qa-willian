Given(/^I should see "(.*?)"$/) do |context|  
  if not element_exists("* {text CONTAINS[c] '#{context}'}")
  	fail(...)
  end
  #print success if false
  #.. 
end

Given(/^I shouldn't see "(.*?)"$/) do |context|  
  if element_exists("* {text CONTAINS[c] '#{context}'}")
  	fail(...)
  end
  #print success if true
  #..
end
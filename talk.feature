@talk
Feature: Talk	

  	@add_message
	Scenario: User should be able to add message
		
		Given I'm talking with "somebody" at chat			 	
		When I write a "test message"
	    And I touch in "send button"						
		Then I should see "test message"

    @delete_message
	Scenario: User should be able to delete message
		
		Given I've message at chat
		And I select "one of the message"
		And I touch in "remove icon"
		When I touch in "delete button"					
		Then I shouldn't see "message"		